fn find_ball(grid: Vec<Vec<i32>>) -> Vec<i32> {
    let balls = grid[0].len();
    let mut res = vec![-1; balls];

    (0..balls).for_each(|ball| {
        let mut pos = ball;
        let mut stuck = false;
        for row in grid.iter() {
            if let Some(npos) = can_fall(pos, row) {
                pos = npos as usize;
            } else {
                stuck = true;
                break;
            }
        }
        if !stuck {
            res[ball] = pos as i32;
        }
    });

    res
}

fn can_fall(pos: usize, row: &[i32]) -> Option<i32> {
    //
    let mut res = None;
    let len = row.len();
    match row[pos] {
        1 => {
            if pos < len.saturating_sub(1) {
                let right = row[pos + 1];
                if right > 0 {
                    res = Some(pos as i32 + 1);
                }
            }
        }
        _ => {
            if pos > 0 {
                let left = row[pos - 1];
                if left < 0 {
                    res = Some(pos as i32 - 1);
                }
            }
        }
    }
    res
}

fn main() {
    let res = find_ball(vec![
        vec![1, 1, 1, -1, -1],
        vec![1, 1, 1, -1, -1],
        vec![-1, -1, -1, 1, 1],
        vec![1, 1, 1, 1, -1],
        vec![-1, -1, -1, -1, -1],
    ]);

    dbg!(res);
}
