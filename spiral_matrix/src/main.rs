struct Solution();

impl Solution {
    pub fn spiral_order(matrix: &[&[i32]]) -> Vec<i32> {
        let mut fr = 0;
        let mut lr = matrix.len() - 1;
        let mut fc = 0;
        let mut lc = matrix.get(0).unwrap().len() - 1;
        let mut spiral = Vec::with_capacity((lr + 1) * (lc + 1));
        let cols = lc + 1;
        let size = matrix.len() * cols;
        loop {
            dbg!(fr, lr, fc, lc);
            // right
            for i in fc..=lc {
                spiral.push(matrix[fr][i]);
            }
            fr += 1;
            if spiral.len() == size {}

            // down
            for row in fr..=lr {
                spiral.push(matrix[row][lc]);
            }
            lc = lc.saturating_sub(1);
            if spiral.len() == size {
                break;
            }

            // left
            if fr <= lr {
                for e in (fc..=lc).rev() {
                    spiral.push(matrix[lr][e]);
                }
            }
            lr = lr.saturating_sub(1);
            if spiral.len() == size {
                break;
            }

            // up
            if fc <= lc {
                for i in (fr..=lr).rev() {
                    spiral.push(matrix[i][fc]);
                }
            }
            fc += 1;
            if spiral.len() == size {
                break;
            }

            dbg!(&spiral);
        }

        spiral
    }
}

fn main() {
    dbg!(Solution::spiral_order(&[
        &[1, 2, 3],
        &[4, 5, 6],
        &[7, 8, 9]
    ]));

    dbg!(Solution::spiral_order(&[&[3], &[2]]));
}
