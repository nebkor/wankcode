struct Solution;

enum Zeros {
    None,
    One(usize),
    Many,
}

impl Solution {
    pub fn product_except_self(nums: Vec<i32>) -> Vec<i32> {
        let mut out = nums;
        let nums = &mut out;
        let zeros = count_zeros(nums);

        match zeros {
            Zeros::None => {
                let prod: i32 = nums.iter().product();
                for x in nums.iter_mut() {
                    *x = prod / *x;
                }
            }
            Zeros::One(z) => {
                let (left, right) = nums.split_at(z);
                let left = if z == 0 { 1 } else { left.iter().product() };
                let right = if z == nums.len() - 1 {
                    1
                } else {
                    right[1..].iter().product()
                };
                for x in nums.iter_mut() {
                    *x = 0;
                }
                nums[z] = left * right;
            }
            _ => {}
        }
        out
    }
}

fn count_zeros(nums: &[i32]) -> Zeros {
    let mut out = Zeros::None;
    let zs = 0;
    for (i, n) in nums.iter().enumerate() {
        if *n == 0i32 {
            if zs == 0 {
                out = Zeros::One(i);
            } else {
                out = Zeros::Many;
                break;
            }
        }
    }
    out
}

fn main() {
    let p = Solution::product_except_self(vec![-1, 1, 0, -3, 3]);
    dbg!(p);
}
