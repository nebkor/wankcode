use aoc_runner_derive::{aoc as aoc_run, aoc_generator};
use std::collections::HashSet;

type Beep = HashSet<char>;

#[aoc_generator(day6)]
fn parse_input_day1(input: &str) -> String {
    input.to_string()
}

#[aoc_run(day6, part1)]
fn part1(input: &str) -> usize {
    get_token(input.trim(), 4)
}

#[aoc_run(day6, part2)]
fn part2(input: &str) -> usize {
    get_token(input.trim(), 14)
}

fn get_token(input: &str, len: usize) -> usize {
    let mut out = 0;
    for (i, c) in input.as_bytes().windows(len).enumerate() {
        let window = Beep::from_iter(c.iter().map(|c| *c as char));
        out = i + len;
        if window.len() == len {
            break;
        }
    }
    out
}
