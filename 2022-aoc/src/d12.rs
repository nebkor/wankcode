use aoc_runner_derive::{aoc as aoc_run, aoc_generator};
use petgraph::{algo::dijkstra, prelude::NodeIndex, Graph};

type GRAPH = Graph<char, ()>;

type Pos = (usize, usize);

#[aoc_generator(day12)]
fn parse_input(input: &str) -> (NodeIndex, NodeIndex, GRAPH) {
    let mut out = Graph::default();
    let mut g = Vec::new();
    let mut s = NodeIndex::default();
    let mut e = NodeIndex::default();

    for line in input.lines() {
        let row = Vec::from_iter(line.chars());
        g.push(row);
    }

    let rows = g.len();
    let cols = g[0].len();
    let mut g2 = vec![vec![NodeIndex::default(); cols]; rows];
    for row in 0..rows {
        for col in 0..cols {
            let weight = g[row][col];
            let node = out.add_node(weight);
            g2[row][col] = node;
            if weight == 'S' {
                s = node;
            }
            if weight == 'E' {
                e = node;
            }
        }
    }

    for row in 0..rows {
        for col in 0..cols {
            let node = g2[row][col];
            for neighbor in neighbors((row, col), &g).iter() {
                let &(row, col) = neighbor;
                let neighbor = g2[row][col];
                out.add_edge(node, neighbor, ());
            }
        }
    }

    (s, e, out)
}

fn neighbors((ridx, cidx): Pos, graph: &[Vec<char>]) -> Vec<(usize, usize)> {
    let n = graph[ridx][cidx];
    let mut out = Vec::new();
    for (dr, dc) in [(-1, 0), (1, 0), (0, -1), (0, 1)] {
        let row = (dr + ridx as i32) as usize;
        let col = (dc + cidx as i32) as usize;
        if let Some(r) = graph.get(row) {
            if let Some(c) = r.get(col) {
                if reachable(n, *c) {
                    out.push((row, col));
                }
            }
        }
    }

    out
}

fn reachable(a: char, b: char) -> bool {
    let a = match a {
        'S' => 'a',
        'E' => 'z',
        _ => a,
    };
    let b = match b {
        'S' => 'a',
        'E' => 'z',
        _ => b,
    };
    (a as u8 + 1) >= b as u8
}

#[aoc_run(day12, part1)]
fn part1((start, end, graph): &(NodeIndex, NodeIndex, GRAPH)) -> i32 {
    let res = dijkstra(graph, *start, Some(*end), |_| 1);
    res[end]
}

#[aoc_run(day12, part2)]
fn part2((start, end, graph): &(NodeIndex, NodeIndex, GRAPH)) -> i32 {
    let mut roots = Vec::new();
    roots.push(*start);
    for (i, w) in graph.node_weights().enumerate() {
        if w == &'a' {
            roots.push(NodeIndex::new(i));
        }
    }
    let mut res = i32::MAX;
    for root in roots {
        let p = dijkstra(graph, root, Some(*end), |_| 1);
        if let Some(&v) = p.get(end) {
            res = res.min(v);
        }
    }
    res
}

/*
#[cfg(test)]
mod test {
    use super::*;
    const INPUT: &str = "Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi";

    #[test]
    fn part1_test() {
        let v = parse_input(INPUT);

        assert_eq!(part1(&v), 31);
    }

    #[test]
    fn part2_test() {
        let v = parse_input(INPUT);
        assert_eq!(part2(&v), 29);
    }
}
 */
