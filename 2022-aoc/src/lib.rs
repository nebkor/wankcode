use aoc_runner_derive::aoc_lib;

mod d1;
mod d2;
mod d3;
mod d4;
mod d5;
mod d6;
mod d7;
mod d8;
mod d9;
mod d10;
mod d11;
mod d12;
mod d13;

aoc_lib! { year = 2022 }
