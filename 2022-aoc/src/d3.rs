use std::collections::HashSet;

use aoc_runner_derive::{aoc as aoc_run, aoc_generator};

type Contents = (HashSet<char>, HashSet<char>);

fn get_priority(item: &char) -> u32 {
    if item.is_ascii_lowercase() {
        *item as u32 - 96
    } else {
        *item as u32 - 38
    }
}

#[aoc_generator(day3, part1)]
fn parse_input(input: &str) -> Vec<Contents> {
    let mut out = Vec::with_capacity(input.len());
    for line in input.lines() {
        let half_len = line.len() / 2;
        let lo = HashSet::from_iter(line[0..half_len].chars());
        let hi = HashSet::from_iter(line[half_len..].chars());
        out.push((lo, hi));
    }

    out
}

#[aoc_generator(day3, part2)]
fn parse_input2(input: &str) -> Vec<u32> {
    let mut out = Vec::with_capacity(input.len() / 3);
    let lines: Vec<&str> = input.lines().collect();
    for group in lines.chunks_exact(3) {
        let [x, y, z] = group else { panic!() };
        let x: HashSet<char> = HashSet::from_iter(x.chars());
        let y = HashSet::from_iter(y.chars());
        let z = HashSet::from_iter(z.chars());
        let xy = &x & &y;
        let common = xy.intersection(&z).next().unwrap();
        out.push(get_priority(common));
    }

    out
}

#[aoc_run(day3, part1)]
fn part1(sacks: &[Contents]) -> u32 {
    let mut out = 0;
    for (lo, hi) in sacks {
        let common = lo.intersection(hi).next().unwrap();
        out += get_priority(common);
    }
    out
}

#[aoc_run(day3, part2)]
fn p2(items: &[u32]) -> u32 {
    items.iter().sum()
}
