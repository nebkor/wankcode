use aoc_runner_derive::{aoc as aoc_run, aoc_generator};

fn score(turn: &str) -> u32 {
    let mv = turn.split(' ').collect::<Vec<_>>()[1];
    let mut out = match mv {
        "X" => 1,
        "Y" => 2,
        "Z" => 3,
        _ => unreachable!(),
    };

    match turn {
        "A Y" | "B Z" | "C X" => out += 6,
        "A X" | "B Y" | "C Z" => out += 3,
        _ => {}
    }
    out
}

fn s2(turn: &str) -> u32 {
    let wld = turn.split(' ').collect::<Vec<_>>()[1];
    let mut out = match wld {
        "X" => 0,
        "Y" => 3,
        "Z" => 6,
        _ => unreachable!(),
    };

    let mv = turn.split(' ').collect::<Vec<_>>()[0];

    out += match mv {
        //
        "A" => match out {
            0 => 3,
            3 => 1,
            6 => 2,
            _ => 0,
        },
        "B" => match out {
            0 => 1,
            3 => 2,
            6 => 3,
            _ => 0,
        },
        "C" => match out {
            0 => 2,
            3 => 3,
            6 => 1,
            _ => 0,
        },
        _ => 0,
    };
    out
}

#[aoc_generator(day2, part1)]
fn parse_input(input: &str) -> Vec<u32> {
    let mut out = Vec::new();
    for line in input.lines() {
        out.push(score(line));
    }
    out
}

#[aoc_generator(day2, part2)]
fn parse_input2(input: &str) -> Vec<u32> {
    let mut out = Vec::new();
    for line in input.lines() {
        out.push(s2(line));
    }
    out
}

#[aoc_run(day2, part1)]
fn part1(scores: &[u32]) -> u32 {
    scores.iter().sum()
}

#[aoc_run(day2, part2)]
fn part2(scores: &[u32]) -> u32 {
    scores.iter().sum()
}
