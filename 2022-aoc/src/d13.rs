use aoc_runner_derive::{aoc as aoc_run, aoc_generator};
use lyn::Scanner;

/*
The following grammar can be used at https://bnfplayground.pauliankline.com/ to play with matching

<packet> ::= <leftbrace> <list>? <rightbrace>
<list> ::= <element> (<comma> <element>)*
<element> ::= <number> | <leftbrace> <list>* <rightbrace>
<comma> ::= "," | ", "
<leftbrace> ::= "["
<rightbrace> ::= "]"
<number> ::= [0-9]+
*/

#[derive(PartialEq, Eq, PartialOrd, Ord, Debug, Clone)]
struct Packet {
    elements: Vec<Element>,
}

#[derive(Debug, PartialEq, Eq, Clone)]
enum Element {
    Plain(u32),
    List(Vec<Element>),
}

impl PartialOrd for Element {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        use Element::*;
        match (self, other) {
            (Plain(v1), Plain(v2)) => Some(v1.cmp(v2)),
            (List(v1), List(v2)) => v1.partial_cmp(v2),
            (Plain(_), List(_)) => List(vec![self.clone()]).partial_cmp(other),
            (List(_), Plain(_)) => self.partial_cmp(&List(vec![other.clone()])),
        }
    }
}

impl Ord for Element {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.partial_cmp(other).unwrap()
    }
}

#[aoc_generator(day13)]
fn parse_input(input: &str) -> Vec<Packet> {
    let mut out = Vec::new();
    for line in input.lines() {
        if line.trim().is_empty() {
            continue;
        }
        let mut scanner = Scanner::new(line);
        let packet = parse_packet(&mut scanner);
        out.push(packet);
    }

    out
}

fn parse_packet(scanner: &mut Scanner) -> Packet {
    // we can pop the first character off, it's the opening brace
    scanner.pop();
    let elements = parse_list(scanner);
    Packet { elements }
}

fn parse_list(scanner: &mut Scanner) -> Vec<Element> {
    let mut out = Vec::new();
    while !scanner.is_done() {
        match scanner.peek().unwrap() {
            '[' => {
                scanner.pop();
                let v = parse_list(scanner);
                out.push(Element::List(v));
            }
            c if c.is_ascii_digit() => {
                //
                let mut s = String::new();
                while let Some(c) = scanner.peek() {
                    if c.is_ascii_digit() {
                        let c = scanner.pop().unwrap();
                        s.push(*c);
                    } else {
                        break;
                    }
                }
                let num: u32 = s.parse().unwrap();
                out.push(Element::Plain(num));
            }
            ']' => {
                scanner.pop();
                break;
            }
            _ => {
                scanner.pop();
            }
        };
    }

    out
}

#[aoc_run(day13, part1)]
fn part1(input: &[Packet]) -> u32 {
    let mut out = 0;
    for (i, packets) in input.chunks(2).enumerate() {
        //
        let (left, right) = (&packets[0], &packets[1]);
        if left < right {
            out += i + 1;
        }
    }
    out as u32
}

#[aoc_run(day13, part2)]
fn part2(input: &[Packet]) -> u32 {
    let d1 = &parse_packet(&mut Scanner::new("[[2]]"));
    let d2 = &parse_packet(&mut Scanner::new("[[6]]"));
    let mut input = input.to_vec();
    input.push(d1.clone());
    input.push(d2.clone());
    input.sort();
    let mut out = 1;

    for (i, p) in input.iter().enumerate() {
        if p == d1 || p == d2 {
            out *= i + 1;
        }
    }

    out as u32
}

/*
#[cfg(test)]
mod test {
    use super::*;
    const INPUT: &str = "[1,1,3,1,1]
[1,1,5,1,1]

[[1],[2,3,4]]
[[1],4]

[9]
[[8,7,6]]

[[4,4],4,4]
[[4,4],4,4,4]

[7,7,7,7]
[7,7,7]

[]
[3]

[[[]]]
[[]]

[1,[2,[3,[4,[5,6,7]]]],8,9]
[1,[2,[3,[4,[5,6,0]]]],8,9]";

    #[test]
    fn part1_test() {
        let v = parse_input(INPUT);

        assert_eq!(part1(&v), 13);
    }

    #[test]
    fn part2_test() {
        let v = parse_input(INPUT);
        assert_eq!(part2(&v), 140);
    }
}
*/
