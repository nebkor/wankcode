use aoc_runner_derive::{aoc as aoc_run, aoc_generator};
use std::collections::BinaryHeap;

type Beep = BinaryHeap<u32>;

#[aoc_generator(day1)]
fn parse_input_day1(input: &str) -> Beep {
    let mut cur = 0;
    let mut out = BinaryHeap::new();
    for line in input.lines() {
        if let Ok(n) = line.parse::<u32>() {
            cur += n;
        } else {
            out.push(cur);
            cur = 0;
        }
    }
    out.push(cur);
    out
}

#[aoc_run(day1, part1)]
fn part1(cals: &Beep) -> u32 {
    let mut cals = cals.to_owned();
    cals.pop().unwrap()
}

#[aoc_run(day1, part2)]
fn p2(cals: &Beep) -> u32 {
    let mut cals = cals.to_owned();
    let mut out = 0;
    for _ in 0..3 {
        out += cals.pop().unwrap();
    }
    out
}
