use aoc_runner_derive::{aoc as aoc_run, aoc_generator};

#[derive(Debug, Clone, PartialEq, Eq)]
enum Instr {
    Noop,
    AddX(i32, u8),
}

impl Instr {
    pub fn execute(&mut self) -> Option<i32> {
        match self {
            Instr::AddX(val, done) => {
                if *done == 0 {
                    Some(*val)
                } else {
                    *done -= 1;
                    None
                }
            }
            Instr::Noop => None,
        }
    }

    pub fn addx(val: i32) -> Self {
        Instr::AddX(val, 1)
    }

    pub fn is_noop(&self) -> bool {
        self == &Instr::Noop
    }
}

#[derive(Debug, Clone)]
struct Vm {
    cycle: usize,
    x: i32,
    ip: usize,
    instrs: Vec<Instr>,
}

impl Vm {
    pub fn new(instrs: Vec<Instr>) -> Self {
        Vm {
            cycle: 1,
            x: 1,
            ip: 0,
            instrs,
        }
    }

    pub fn run(&mut self) -> (usize, i32) {
        let instr = self.instrs.get_mut(self.ip).unwrap();
        let out = (self.cycle, self.x);
        let mut incr = false;
        if let Some(v) = instr.execute() {
            self.x += v;
            incr = true;
        }
        if instr.is_noop() {
            incr = true;
        }
        if incr {
            self.ninst();
        }
        self.cycle += 1;
        out
    }

    fn ninst(&mut self) {
        self.ip = (self.ip + 1).min(self.instrs.len() - 1);
    }
}

fn cycle2beam(cycle: usize) -> (usize, usize) {
    let col = (cycle - 1) % 40;
    let row = (cycle - 1) / 40;
    (row, col)
}

#[aoc_generator(day10)]
fn parse_input(input: &str) -> Vm {
    let mut data = Vec::new();
    for line in input.lines() {
        match line {
            "noop" => data.push(Instr::Noop),
            _ => {
                let (_, v) = line.split_once(' ').unwrap();
                data.push(Instr::addx(v.parse().unwrap()));
            }
        }
    }
    Vm::new(data)
}

#[aoc_run(day10, part1)]
fn part1(input: &Vm) -> i32 {
    let mut out = 0;
    let mut vm = input.clone();
    let mut samp = 20;
    for _ in 0..221 {
        let (cycle, x) = vm.run();
        if cycle % samp == 0 {
            samp += 40;
            out += cycle as i32 * x;
        }
    }
    out
}

#[aoc_run(day10, part2)]
fn part2(input: &Vm) -> String {
    let mut vm = input.clone();
    let mut crt = vec![vec!['.'; 40]; 6];

    for _ in 0..240 {
        let (cycle, x) = vm.run();
        let x = (x - 1)..=(x + 1);
        let (row, col) = cycle2beam(cycle);
        if x.contains(&(col as i32)) {
            crt[row][col] = '#';
        }
    }

    let mut out = String::with_capacity(248);
    out.push('\n');
    for row in crt {
        for char in row.iter() {
            out.push(*char);
        }
        out.push('\n');
    }
    out
}

/*
#[cfg(test)]
mod tests {
    use super::*;

    const INPUT: &str = "addx 15
addx -11
addx 6
addx -3
addx 5
addx -1
addx -8
addx 13
addx 4
noop
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx -35
addx 1
addx 24
addx -19
addx 1
addx 16
addx -11
noop
noop
addx 21
addx -15
noop
noop
addx -3
addx 9
addx 1
addx -3
addx 8
addx 1
addx 5
noop
noop
noop
noop
noop
addx -36
noop
addx 1
addx 7
noop
noop
noop
addx 2
addx 6
noop
noop
noop
noop
noop
addx 1
noop
noop
addx 7
addx 1
noop
addx -13
addx 13
addx 7
noop
addx 1
addx -33
noop
noop
noop
addx 2
noop
noop
noop
addx 8
noop
addx -1
addx 2
addx 1
noop
addx 17
addx -9
addx 1
addx 1
addx -3
addx 11
noop
noop
addx 1
noop
addx 1
noop
noop
addx -13
addx -19
addx 1
addx 3
addx 26
addx -30
addx 12
addx -1
addx 3
addx 1
noop
noop
noop
addx -9
addx 18
addx 1
addx 2
noop
noop
addx 9
noop
noop
noop
addx -1
addx 2
addx -37
addx 1
addx 3
noop
addx 15
addx -21
addx 22
addx -6
addx 1
noop
addx 2
addx 1
noop
addx -10
noop
noop
addx 20
addx 1
addx 2
addx 2
addx -6
addx -11
noop
noop
noop";

    #[test]
    fn part1_test() {
        let vm = parse_input(INPUT);

        assert_eq!(part1(&vm), 13140);
    }

    #[test]
    fn part2_test() {
        let vm = parse_input(INPUT);
        let expected = "
##..##..##..##..##..##..##..##..##..##..
###...###...###...###...###...###...###.
####....####....####....####....####....
#####.....#####.....#####.....#####.....
######......######......######......####
#######.......#######.......#######.....
";
        let out = part2(&vm);
        println!("{}", &out);
        assert_eq!(&out, &expected);
    }

    #[test]
    fn p2_alt_test() {
        let input = "noop
addx 10
addx -4
addx -1
noop
noop
addx 5
addx -12
addx 17
noop
addx 1
addx 2
noop
addx 3
addx 2
noop
noop
addx 7
addx 3
noop
addx 2
noop
noop
addx 1
addx -38
addx 5
addx 2
addx 3
addx -2
addx 2
addx 5
addx 2
addx -4
addx 26
addx -19
addx 2
addx 5
addx -2
addx 7
addx -2
addx 5
addx 2
addx 4
addx -17
addx -23
addx 1
addx 5
addx 3
noop
addx 2
addx 24
addx 4
addx -23
noop
addx 5
addx -1
addx 6
noop
addx -2
noop
noop
noop
addx 7
addx 1
addx 4
noop
noop
noop
noop
addx -37
addx 5
addx 2
addx 1
noop
addx 4
addx -2
addx -4
addx 9
addx 7
noop
noop
addx 2
addx 3
addx -2
noop
addx -12
addx 17
noop
addx 3
addx 2
addx -3
addx -30
addx 3
noop
addx 2
addx 3
addx -2
addx 2
addx 5
addx 2
addx 11
addx -6
noop
addx 2
addx -19
addx 20
addx -7
addx 14
addx 8
addx -7
addx 2
addx -26
addx -7
noop
noop
addx 5
addx -2
addx 5
addx 15
addx -13
addx 5
noop
noop
addx 1
addx 4
addx 3
addx -2
addx 4
addx 1
noop
addx 2
noop
addx 3
addx 2
noop
noop
noop
noop
noop";
        let mut vm = parse_input(input);
        let out = part2(&vm);
        let expected = "
###...##...##..####.#..#.#....#..#.####.
#..#.#..#.#..#.#....#.#..#....#..#.#....
###..#..#.#....###..##...#....####.###..
#..#.####.#....#....#.#..#....#..#.#....
#..#.#..#.#..#.#....#.#..#....#..#.#....
###..#..#..##..####.#..#.####.#..#.#....
";
        assert_eq!(out, expected);
    }
}
*/
