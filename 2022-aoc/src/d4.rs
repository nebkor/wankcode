use aoc_runner_derive::{aoc as aoc_run, aoc_generator};

type TRange = (u32, u32);
type Assignments = (TRange, TRange);

#[aoc_generator(day4)]
fn parse_input(input: &str) -> Vec<Assignments> {
    let mut out = Vec::with_capacity(input.len());
    for line in input.lines() {
        let (a, b) = line.split_once(',').unwrap();
        let assignment = (get_range(a), get_range(b));
        out.push(assignment);
    }

    out
}

#[aoc_run(day4, part1)]
fn part1(scheds: &[Assignments]) -> u32 {
    let mut out = 0;
    for (a, b) in scheds {
        if contains(a, b) || contains(b, a) {
            out += 1;
        }
    }
    out
}

#[aoc_run(day4, part2)]
fn part2(scheds: &[Assignments]) -> u32 {
    let mut out = 0;
    for (a, b) in scheds {
        if overlaps(a, b) || overlaps(b, a) {
            out += 1;
        }
    }
    out
}

fn get_range(r: &str) -> TRange {
    let (lo, hi) = r.split_once('-').unwrap();
    (lo.parse().unwrap(), hi.parse().unwrap())
}

fn contains(a: &TRange, b: &TRange) -> bool {
    a.0 <= b.0 && b.1 <= a.1
}

fn overlaps(a: &TRange, b: &TRange) -> bool {
    a.0 <= b.0 && b.0 <= a.1
}
