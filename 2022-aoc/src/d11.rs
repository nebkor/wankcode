use std::collections::VecDeque;

use aoc_runner_derive::{aoc as aoc_run, aoc_generator};

#[derive(Debug, Clone, Default)]
struct Monkey {
    items: VecDeque<u128>,
    op: Operation,
    tmod: u128,
    target: (usize, usize),
    business: u128,
}

#[derive(Clone, Debug, PartialEq, Eq)]
enum Operation {
    Add(Option<u128>),
    Mul(Option<u128>),
}

impl Default for Operation {
    fn default() -> Self {
        Operation::Add(Some(0))
    }
}

impl Monkey {
    pub fn inspect(&mut self, reducer: &dyn Fn(u128) -> u128) -> Vec<(usize, u128)> {
        let mut out = Vec::with_capacity(self.items.len());
        while let Some(item) = self.items.pop_front() {
            let item = reducer(self.op(item));
            if item % self.tmod == 0 {
                out.push((self.target.0, item));
            } else {
                out.push((self.target.1, item));
            }
            self.business += 1;
        }
        self.items.clear();
        out
    }

    fn op(&self, item: u128) -> u128 {
        match self.op {
            Operation::Add(Some(v)) => item + v,
            Operation::Add(None) => item + item,
            Operation::Mul(Some(v)) => item * v,
            Operation::Mul(None) => item * item,
        }
    }
}

#[aoc_generator(day11)]
fn parse_input(input: &str) -> Vec<Monkey> {
    let mut out = Vec::new();
    let mut monkey = Monkey::default();

    for line in input.lines() {
        let monkey = &mut monkey;
        let line = line.trim();

        if line.starts_with("Monkey") {
            monkey.business = 0;
            continue;
        }
        if line.starts_with("Starting") {
            monkey.items = parse_items(line);
            continue;
        }
        if line.starts_with("Operation") {
            monkey.op = parse_operation(line);
            continue;
        }
        if line.starts_with("Test") {
            monkey.tmod = line.split_whitespace().last().unwrap().parse().unwrap();
            continue;
        }
        if line.starts_with("If true") {
            monkey.target.0 = line.split_whitespace().last().unwrap().parse().unwrap();
            continue;
        }
        if line.starts_with("If false") {
            monkey.target.1 = line.split_whitespace().last().unwrap().parse().unwrap();
            let m = monkey.clone();
            out.push(m);
            *monkey = Monkey::default();
            continue;
        }
    }

    out
}

fn parse_items(items: &str) -> VecDeque<u128> {
    let mut out = VecDeque::new();
    let (_, items) = items.split_once(" items: ").unwrap();
    for item in items.split(", ") {
        out.push_back(item.parse().unwrap());
    }
    out
}

fn parse_operation(op: &str) -> Operation {
    let (_, op) = op.split_once(" new = old ").unwrap();
    let (op, val) = op.split_once(' ').unwrap();
    match op {
        "*" => Operation::Mul(val.parse().ok()),
        "+" => Operation::Add(val.parse().ok()),
        _ => unreachable!(),
    }
}

#[aoc_run(day11, part1)]
fn part1(troop: &[Monkey]) -> u128 {
    let mut troop = troop.to_owned();
    for _round in 0..20 {
        for i in 0..troop.len() {
            let monkey = &mut troop[i];
            let items = monkey.inspect(&|item| item / 3);
            for (target, item) in items {
                let m = &mut troop[target];
                m.items.push_back(item);
            }
        }
    }
    troop.sort_by(|m1, m2| m2.business.cmp(&m1.business));
    troop.iter().take(2).map(|m| m.business).product()
}

#[aoc_run(day11, part2)]
fn part2(troop: &[Monkey]) -> u128 {
    let max: u128 = troop.iter().map(|m| m.tmod).product();
    let mut troop = troop.to_owned();
    for _round in 0..10_000 {
        for i in 0..troop.len() {
            let monkey = &mut troop[i];
            let items = monkey.inspect(&|item| item % max);
            for (target, item) in items {
                let m = &mut troop[target];
                m.items.push_back(item);
            }
        }
    }
    troop.sort_by(|m1, m2| m2.business.cmp(&m1.business));
    troop.iter().take(2).map(|m| m.business).product()
}

/*
#[cfg(test)]
mod test {
    use super::*;
    const INPUT: &str = "Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1";

    #[test]
    fn part1_test() {
        let v = parse_input(INPUT);

        assert_eq!(part1(&v), 10605);
    }

    #[test]
    fn part2_test() {
        let v = parse_input(INPUT);
        assert_eq!(part2(&v), 2713310158);
    }
}
*/
