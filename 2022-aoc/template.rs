use aoc_runner_derive::{aoc as aoc_run, aoc_generator};

type INPUT = char;

#[aoc_generator(dayDAY)]
fn parse_input(input: &str) -> Vec<INPUT> {
    todo!()
}

#[aoc_run(dayDAY, part1)]
fn part1(input: &[INPUT]) -> u32 {
    0
}

#[aoc_run(dayDAY, part2)]
fn part2(input: &[INPUT]) -> u32 {
    0
}

#[cfg(test)]
mod test {
    use super::*;
    const INPUT: &str = "";

    #[test]
    fn part1_test() {
        let v = parse_input(INPUT);

        assert_eq!(part1(&v), 1);
    }

    #[test]
    fn part2_test() {
        let v = parse_input(INPUT);
        assert_eq!(part2(&v), 1);
    }
}
