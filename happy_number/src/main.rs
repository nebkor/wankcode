struct Solution {}

impl Solution {
    pub fn is_happy(n: i32) -> bool {
        let mut s = std::collections::HashSet::new();
        let mut n = n;
        while s.insert(n) {
            n = Self::sum_squares(n);
        }
        n == 1
    }

    fn sum_squares(n: i32) -> i32 {
        n.to_string()
            .chars()
            .map(|c| c.to_digit(10).unwrap_or(0).pow(2))
            .sum::<u32>() as i32
    }
}

fn main() {
    println!("{}", Solution::is_happy(19));
}
